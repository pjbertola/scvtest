//
//  ViewController.h
//  SCVTest
//
//  Created by Pablo J. Bertola on 2/11/16.
//  Copyright © 2016 Pablo J. Bertola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@import MapKit;

@interface ViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

