//
//  ViewController.m
//  SCVTest
//
//  Created by Pablo J. Bertola on 2/11/16.
//  Copyright © 2016 Pablo J. Bertola. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) CLLocationManager *manager;
@property (strong, nonatomic) CLCircularRegion *cirRegion;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    CLLocationCoordinate2D c2d;
    
    
    c2d.longitude = -122.047842;
    c2d.latitude = 37.334210;

//    MKAnnotationView *pin = [[MKAnnotationView alloc]init];
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = c2d;
    
    [self.mapView addAnnotation:annotation];
    
    
    
//    MKCoordinateRegion pin = MKCoordinateRegionMakeWithDistance(c2d, 100, 100);
//    
//    self.mapView ad
    
    self.mapView.userTrackingMode = YES;
    
    self.manager = [[CLLocationManager alloc]init];
    
    self.manager.delegate = self;
    
    self.manager.desiredAccuracy = kCLLocationAccuracyBest;
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusNotDetermined) {
         [self.manager requestAlwaysAuthorization];
    }else if (status != kCLAuthorizationStatusDenied && status != kCLAuthorizationStatusRestricted)
    {
        
        

        [self.manager startUpdatingLocation];
        
        
//        37.334210 - longitud: -122.047842


        
//        latitud: -34.602577 - longitud: -58.454468
    
        self.cirRegion = [[CLCircularRegion alloc]initWithCenter:c2d radius:100 identifier:@"SVC"];
    
    
        [self.manager startMonitoringForRegion:self.cirRegion];
    
    }

    

}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
            [manager startUpdatingLocation];
        CLLocationCoordinate2D c2d;
        
        c2d.longitude = -122.047842;
        c2d.latitude = 37.334210;
        self.cirRegion = [[CLCircularRegion alloc]initWithCenter:c2d radius:100 identifier:@"SVC"];
        
        
        [self.manager startMonitoringForRegion:self.cirRegion];
    }


}
-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(nullable CLRegion *)region withError:(nonnull NSError *)error
{
    NSLog([error localizedDescription]);
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog([error localizedDescription]);
}


-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"enter region");
    [self postToSlack:@"Enter Region SVC iOS"];
}
-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"enter region");
    [self postToSlack:@"Exit Region SVC iOS. Bye bye"];
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"update location");
    
    CLLocation *location = [locations firstObject];
    
    NSLog(@"latitud: %f - longitud: %f", location.coordinate.latitude, location.coordinate.longitude);
    
    //latitud: -34.602577 - longitud: -58.454468
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)postToSlack:(NSString *)textStr
{
    NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:textStr, @"text",
                                @"Pablo J. Bertola", @"username",
                                @"https://avatars.slack-edge.com/2015-07-07/7349085092_8312ade20e4c8d6327f6_192.jpg", @"icon_url",nil];
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0//(NSJSONWritingOptions) (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    //    [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"api-key"       : @"API_KEY",
                                                   @"Content-Type"  : @"application/json"
                                                   };
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:@"https://hooks.slack.com/services/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"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPBody = jsonData;
    
    request.HTTPMethod = @"POST";
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // The server answers with an error because it doesn't receive the params
        
        if (data) {
            
            
            NSLog([[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
            
        }else
        {
            NSLog([error localizedDescription]);
        }
        
    }];
    [postDataTask resume];
    
    

}

@end
