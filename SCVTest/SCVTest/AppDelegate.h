//
//  AppDelegate.h
//  SCVTest
//
//  Created by Pablo J. Bertola on 2/11/16.
//  Copyright © 2016 Pablo J. Bertola. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

